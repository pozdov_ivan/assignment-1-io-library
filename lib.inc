%define sys_exit 60
%define sys_read 0 
%define sys_write 1

%define ascii_space 0x20
%define ascii_tab 0x9
%define ascii_lf 0xA

%define stdin_fd 0
%define stdout_fd 1

%macro space_test 1
    cmp rax, ascii_space
    je %1
    cmp rax, ascii_tab
    je %1
    cmp rax, ascii_lf
    je %1
%endmacro

%macro numeric_test 1
    cmp cl, "0"
    jl %1
    cmp cl, "9"
    jg %1
%endmacro

section .text 
 
; Принимает код возврата и завершает текущий процесс
exit:  
    mov rax, sys_exit
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    cmp byte[rdi+rax], 0
    je .end
    inc rax
    jmp .loop
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rdx, rax
    mov rsi, rdi
    mov rdi, stdout_fd
    mov rax, sys_write
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, ascii_lf

; Принимает код символа и выводит его в stdout
print_char:
    dec rsp
    mov [rsp], dil
    mov rsi, rsp 
    mov rdx, 1
    mov rdi, stdout_fd
    mov rax, sys_write
    syscall 
    inc rsp
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r8, 10
    mov r9, 1
    dec rsp
    mov [rsp], byte 0
.loop:
    xor rdx, rdx
    div r8 
    add rdx, "0"
    dec rsp
    inc r9
    mov [rsp], dl
    test rax, rax
    jz .end
    jnz .loop
.end:
    mov rdi, rsp
    push r9
    call print_string
    pop r9
    add rsp, r9
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns print_uint
    neg rdi
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
.loop:
    mov cl, byte[rdi+rax] 
    cmp cl, byte[rsi+rax]
    jne .fail
    cmp byte[rdi+rax], 0
    je .success
    inc rax
    jmp .loop
.fail:
    xor rax, rax
    ret
.success:
    mov rax, 1
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0x0            
    xor rax, rax        
    mov rdi, stdin_fd        
    mov rsi, rsp        
    mov rdx, 1         
    syscall
    cmp rax, -1
    pop rax            
    jne .end
    xor rax, rax        
.end:
    ret



; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    cmp rsi, 1
    jle .fail
.read_before:
    push rsi
    push rdi
    call read_char
    pop rdi
    pop rsi
    xor r8, r8
    cmp rax, 0
    je .success
    space_test(.read_before)
    mov [rdi], al
    inc r8
.read_after:
    push r8
    push rdi
    push rsi
    call read_char
    pop rsi
    pop rdi
    pop r8
    cmp rax, 0
    je .success
    space_test(.success)
    inc r8
    cmp r8, rsi
    je .fail
    mov [rdi+r8-1], al
    jmp .read_after
.fail:
    xor rax, rax
    ret
.success:
    mov byte[rdi+r8], 0
    mov rax, rdi
    mov rdx, r8
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
.loop:
    mov cl, byte[rdi+rdx]
    cmp cl, 0
    je .end
    cmp cl, "0"
    jl .end
    cmp cl, "9"
    jg .end
    inc rdx
    sub cl, "0"
    imul rax, rax, 10
    add rax, rcx
    jmp .loop
.end:
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rdx, rdx
    xor rax, rax
    cmp byte[rdi], "-"
    je .has_minus
    cmp byte[rdi], "+"
    je .has_plus
    jmp .loop2
.has_minus:
    inc rdx
    neg rax
.loop1:
    mov cl, byte[rdi+rdx]
    numeric_test(.end)
    inc rdx
    sub cl, "0"
    imul rax, rax, 10
    sub rax, rcx
    jmp .loop1
.has_plus:
    inc rdx
.loop2:
    mov cl, byte[rdi+rdx]
    numeric_test(.end)
    inc rdx
    sub cl, "0"
    imul rax, rax, 10
    add rax, rcx
    jmp .loop2
.end:
    ret


; Принимает указатель на строку, указатель на буфер и длину буфера (rdi, rsi, rdx)
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi 
    cmp rax, rdx
    jg .fail
    xor r8, r8
.loop:
    mov r9, [rdi+r8]
    mov [rsi+r8], r9
    inc r8
    cmp r8, rax
    jl .loop
    je .success
.fail:
    xor rax, rax
.success:
    ret   